let someString: any = "Some string";
// операция кастинга
let l: number = (<string>someString).length;

console.log(l);

let value: any = "value";
let size: number = (value as string).length;

console.log(size);
